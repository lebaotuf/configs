					; Emacs Lisp snippets not worthy of inclusion in init.el

;; Org Mode minted config
(setq org-latex-pdf-process
      '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
        "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
        "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f")
      org-latex-listings 'minted)

(add-to-list 'org-latex-packages-alist '("cache=false" "minted"))
(add-to-list 'org-latex-minted-langs '(sql "tsql"))
